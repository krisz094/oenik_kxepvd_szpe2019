﻿using LemmaSharp.Classes;
using PerProgAnime.Entity;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PerProgAnime.Helper
{
    public class Data1Parser
    {
        public static List<Data1Anime> ParseFromFolder(string folderPath, string titleFileName, string synopsisFileName)
        {
            var titlePath = Path.Combine(folderPath, titleFileName);
            var synopsisPath = Path.Combine(folderPath, synopsisFileName);

            var titles = File.ReadAllLines(titlePath).Skip(1);
            var synopses = File.ReadAllLines(synopsisPath).Skip(1); // TIL: https://www.quora.com/What-is-the-plural-of-synopsis

            ConcurrentDictionary<ushort, Data1Anime> animesById = new ConcurrentDictionary<ushort, Data1Anime>();

            var titleRegex = new Regex(@"\(.*\)$");

            Parallel.ForEach(titles, rawTitle =>
            {

                var idAndTitle = rawTitle.Split('|');
                var id = ushort.Parse(idAndTitle[0]);
                var title = titleRegex.Replace(idAndTitle[1], "").Trim();
                var anime = new Data1Anime() { ID = id, Name = title };
                animesById.TryAdd(anime.ID, anime);
            });

            Regex synopsisRegex = new Regex("[;\\\\/:*?\"<>|&',.()—`]");
            Parallel.ForEach(synopses, rawSynopsis =>
            {
                var idAndSynopsis = rawSynopsis.Split('|');
                var id = ushort.Parse(idAndSynopsis[0]);
                var synopsis = synopsisRegex.Replace(idAndSynopsis[1], "").ToLower().Trim();
                if (animesById.ContainsKey(id))
                {
                    animesById[id].Synopsis = synopsis;
                }
            });

            Data1Anime.nData1Anime = animesById.Count();

            return animesById.Values.ToList();
        }
    }
}
