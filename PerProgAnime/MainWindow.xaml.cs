﻿using CsvHelper;
using CsvHelper.Configuration;
using LemmaSharp.Classes;
using PerProgAnime.Entity;
using PerProgAnime.Helper;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PerProgAnime
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string d1Path = @".\data1";
        string d2Path = @".\data2";
        public ObservableCollection<Data1Anime> D1animes { get; set; }
        public ObservableCollection<Data2Anime> D2animes { get; set; }
        private int processedRatings;
        private int allRatings = -1;

        private int processedUsers;
        private int allUsers = -1;

        private bool processingDone = false;

        private ConcurrentDictionary<int, Data2Anime> animesById;
        //private ConcurrentDictionary<ushort, int> animeInOtherSimilars;

        private string log(string msg)
        {
            Dispatcher.Invoke(() =>
            {
                logList.Items.Add("[" + DateTime.Now.ToString("HH:mm:ss") + "] " + msg);
                logList.SelectedIndex = logList.Items.Count - 1;
            });
            return msg;
        }

        void setStatus(string txt)
        {
            Dispatcher.Invoke(() =>
            {
                stateLabel.Content = txt;
            });
        }

        void updRatings()
        {

            Dispatcher.Invoke(() =>
            {
                if (processingDone)
                {
                    stateLabel.Content = "Ready";
                    stateBar.Visibility = Visibility.Hidden;
                }
                else if (allUsers != -1)
                {
                    stateLabel.Content = $"(4/5) Processed users: {processedUsers}/{allUsers} ({Math.Round(processedUsers / (double)allUsers * 100, 2)}%)";
                    stateBar.Value = processedUsers / (double)allUsers * 100;
                }
                else if (allRatings != -1)
                {
                    stateLabel.Content = $"(3/5) Processed ratings: {processedRatings}/{allRatings} ({Math.Round(processedRatings / (double)allRatings * 100, 2)}%)";
                    stateBar.Value = processedRatings / (double)allRatings * 100;
                }
            });
        }

        private bool AnimeFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFilter.Text))
                return true;
            else
                return ((item as Data2Anime).name.IndexOf(txtFilter.Text, StringComparison.OrdinalIgnoreCase) >= 0);
        }

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            log("Program started");
            stateBar.IsIndeterminate = true;

            StartUpdateUI();
            ProcessData1Animes().ContinueWith(res1 =>
            {
                ProcessData2Animes().ContinueWith(res2 =>
                {

                });
            });

            //ProcessData2Animes();
        }

        public Task ProcessData1Animes()
        {
            var process = new Task(() =>
            {

                var folder = d1Path;
                var titleFName = "datatitle-all-share-new.csv";
                var synopsisFName = "datasynopsis-all-share-new.csv";

                var dataFilePath = @".\lemma\full7z-multext-en.lem";
                var stream = File.OpenRead(dataFilePath);
                var lemmatizer = new Lemmatizer(stream);

                setStatus(log("Parsing Anime DB1 into memory") + "...");

                var animes = Data1Parser.ParseFromFolder(folder, titleFName, synopsisFName);

                setStatus(log("Calculating Bag of Words for synopses") + "...");


                Parallel.ForEach(animes, anime =>
                {
                    anime.calcBagOfWords(lemmatizer);
                });

                setStatus(log("Calculating important words based on TF-IDF") + "...");


                Parallel.ForEach(animes, anime =>
                {
                    foreach (var wordWithCount in anime.bagOfWords)
                    {
                        Data1Anime.nAnimesThatContainWord.AddOrUpdate(wordWithCount.Key, 1, (key, val) =>
                        {
                            return val + 1;
                        });
                    }
                });

                Parallel.ForEach(animes, anime =>
                {
                    anime.getImportantWords();
                });

                setStatus(log("Grouping similar animes by story") + "...");


                var d1acount = animes.Count;
                Parallel.For(0, d1acount - 1, i =>
                  {
                      for (int j = i + 1; j < d1acount; j++)
                      {
                          var a1 = animes[i];
                          var a2 = animes[j];
                          Data1Anime.CompareAndGroupIfSimilar(a1, a2);
                      }
                  });

                setStatus(log("Completed processing Anime DB1") + "...");

                D1animes = new ObservableCollection<Data1Anime>(animes);
            });

            process.Start();

            return process;
        }

        public Task ProcessData2Animes()
        {
            var animePath = System.IO.Path.Combine(d2Path, "anime.csv");
            var ratingPath = System.IO.Path.Combine(d2Path, "rating.csv");

            var process = new Task(() =>
            {
                try
                {
                    ConcurrentDictionary<int, List<Data2Rating>> ratingsOfUsers = new ConcurrentDictionary<int, List<Data2Rating>>();
                    animesById = new ConcurrentDictionary<int, Data2Anime>();

                    setStatus(log("(1/5) Reading Anime DB into memory") + "...");

                    parseAnimes(animePath);

                    setStatus(log("(2/5) Reading Rating DB into memory") + "...");

                    ConcurrentBag<Data2Rating> data2RatingsConc = parseRatings(ratingPath);

                    log("(3/5) Grouping ratings by users");

                    data2RatingsConc = groupRatings(ratingsOfUsers, data2RatingsConc);

                    log("(4/5) Checking user ratings one-by-one to process similar animes");

                    groupSimilarD2Animes(ratingsOfUsers);

                    log("(5/5) Checking similar animes for TF-IDF");

                    prepareD2TfIdf();

                    Dispatcher.Invoke(() =>
                    {
                        mainGrid.Children.Remove(plsWaitLabel);
                    });
                    processingDone = true;
                    log("Finished");
                }
                catch (Exception e)
                {
                    log(e.Message);
                }
            }, TaskCreationOptions.LongRunning);
            process.Start();
            return process;
        }

        private void prepareD2TfIdf()
        {
            Parallel.ForEach(D2animes, anime =>
            {
                var similarIds = anime.similars.Keys;
                foreach (var animeId in similarIds)
                {
                    if (animesById.ContainsKey(animeId))
                    {
                        animesById[animeId].IncrementInOtherAnimeSimilarListCount();
                    }
                }
            });
        }

        private void groupSimilarD2Animes(ConcurrentDictionary<int, List<Data2Rating>> ratingsOfUsers)
        {
            Parallel.ForEach(ratingsOfUsers.Values/*.ToList()*/, ratings =>
            {
                // anime ids that the current user thinks are good
                var goods = ratings.Where(rating => rating.rating >= 8)/*.ToList()*/;

                foreach (var goodRating in goods)
                {
                    // one id from the list
                    var currId = goodRating.anime_id;
                    // all the other ids from the list
                    var otherIds = goods.Where(x => x.anime_id != currId).Select(x => x.anime_id)/*.ToList()*/;
                    if (animesById.ContainsKey(currId))
                    {
                        foreach (var id in otherIds)
                        {
                            animesById[currId].similars.AddOrUpdate(id, (a) =>
                            {
                                return 1;
                            }, (animeId, animeCurrentSimilarViewers) =>
                            {
                                return animeCurrentSimilarViewers + 1;
                            });
                        }
                    }
                }

                Interlocked.Increment(ref processedUsers);
            });
        }

        private ConcurrentBag<Data2Rating> groupRatings(ConcurrentDictionary<int, List<Data2Rating>> ratingsOfUsers, ConcurrentBag<Data2Rating> data2RatingsConc)
        {
            Parallel.ForEach(data2RatingsConc, rating =>
            {
                Interlocked.Increment(ref processedRatings);
                ratingsOfUsers.AddOrUpdate(rating.user_id, new List<Data2Rating>(new Data2Rating[] { rating }), (userId, ratingList) =>
                {
                    var newList = new List<Data2Rating>(ratingList);
                    newList.Add(rating);
                    return newList;
                });
            });

            data2RatingsConc = null;

            GC.Collect();
            GC.WaitForPendingFinalizers();

            allUsers = ratingsOfUsers.Count();
            return data2RatingsConc;
        }

        private ConcurrentBag<Data2Rating> parseRatings(string ratingPath)
        {
            var allRatingLines = File.ReadAllLines(ratingPath).ToList();
            allRatingLines.RemoveAt(0);

            ConcurrentBag<Data2Rating> data2RatingsConc = new ConcurrentBag<Data2Rating>();

            Parallel.ForEach(allRatingLines, line =>
            {
                string[] elems = line.Split(',');
                data2RatingsConc.Add(new Data2Rating()
                {
                    user_id = int.Parse(elems[0]),
                    anime_id = ushort.Parse(elems[1]),
                    rating = sbyte.Parse(elems[2])
                });
            });

            allRatingLines = null;
            allRatings = data2RatingsConc.Count();
            Dispatcher.Invoke(() =>
            {
                stateBar.IsIndeterminate = false;
            });
            return data2RatingsConc;
        }

        private void parseAnimes(string animePath)
        {
            using (var reader = new StreamReader(animePath))
            using (var csv = new CsvReader(reader))
            {
                csv.Configuration.BufferSize = 65536;
                csv.Configuration.CultureInfo = CultureInfo.GetCultureInfo("en-us");
                csv.Configuration.Delimiter = ",";

                var parsedD2 = csv.GetRecords<Data2Anime>().ToList().OrderBy(x => x.name);
                foreach (var anime in parsedD2)
                {
                    anime.name = HttpUtility.HtmlDecode(anime.name);
                }
                D2animes = new ObservableCollection<Data2Anime>(parsedD2);

                Dispatcher.Invoke(() =>
                {
                    d2aList.ItemsSource = D2animes;
                    CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(d2aList.ItemsSource);
                    view.Filter = AnimeFilter;
                });

                Parallel.ForEach(D2animes, anime =>
                {
                    animesById.TryAdd(anime.anime_id, anime);
                });
            }
        }

        public Task StartUpdateUI()
        {
            Task updateUI = new Task(() =>
            {
                while (!processingDone)
                {
                    Thread.Sleep(1000);
                    if (processedRatings > 0)
                    {
                        updRatings();
                    }
                }
                updRatings();
            }, TaskCreationOptions.LongRunning);
            updateUI.Start();
            return updateUI;
        }

        private void txtFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(d2aList.ItemsSource).Refresh();
        }

        private double tfidf(int senderAnimeAllOtherLikers, KeyValuePair<ushort, int> rightSideAnimeKVP)
        {
            var rightSideAnimeLikers = rightSideAnimeKVP.Value;
            Data2Anime rightSideAnime = animesById[rightSideAnimeKVP.Key];

            //var tf = 0; // word count of word in document divided by word count of all words = likers of this DIV BY all left's similars likers
            //var idf = 0; // Log10 -> all documents/how many documents does word appear in = animes count DIV BY how many animes similars contain right side id
            double tf = rightSideAnimeLikers / (double)senderAnimeAllOtherLikers;
            double idf = Math.Log10(D2animes.Count() / (double)rightSideAnime.InOtherAnimeSimilarListCount);
            return tf * idf;
        }

        private bool areStringsSimilar(string a, string b)
        {
            var wordsOfA = a.Split(' ');
            var wordsOfB = b.Split(' ');

            return wordsOfB.All(word => a.Contains(word)) || wordsOfA.All(word => b.Contains(word));
        }

        private string standardizeString(string input)
        {
            var regex = new Regex("[;\\\\/:*?\"<>|&',.()—`]");
            return regex.Replace(input.ToLower(), "");
        }

        private Data1Anime getSimilarD1AnimeTo(Data2Anime anime)
        {
            string d2name = standardizeString(anime.name);
            return D1animes.AsParallel().FirstOrDefault(x => areStringsSimilar(d2name, standardizeString(x.Name)));
        }

        private void d2aList_Selected(object sender, RoutedEventArgs e)
        {
            try
            {
                if (processingDone && d2aList.SelectedItem != null)
                {

                    var senderAnime = (Data2Anime)d2aList.SelectedItem;

                    var allSimilarViewers = senderAnime.similars.Values.Sum();
                    var allAnimeCount = D2animes.Count();
                    var senderAnimeAllOtherLikers = senderAnime.similars.Values.Sum();

                    var res = senderAnime.similars
                        .Where(x => x.Key != senderAnime.anime_id && animesById.ContainsKey(x.Key))
                        .OrderByDescending(x => tfidf(senderAnimeAllOtherLikers, x))
                        .Select(x => $"{animesById[x.Key]} (Also liked by {x.Value} viewer{(x.Value > 1 ? "s" : "")})")
                        .ToList();

                    if (res.Count() == 0)
                    {
                        resultList.ItemsSource = new string[] { "No similar animes found in our database :(" };
                    }
                    else
                    {
                        resultList.ItemsSource = res;
                        resultList.SelectedIndex = 0;
                        resultList.ScrollIntoView(resultList.Items[0]);
                    }

                    var res2 = getSimilarD1AnimeTo(senderAnime);
                    if (res2 != null && res2.similars.Count > 0)
                    {
                        synopsisResultList.ItemsSource = res2.similars
                            .OrderByDescending(x => x.Value)
                            .Select(x => x.Key.Name).ToList();
                        synopsisResultList.SelectedIndex = 0;
                        synopsisResultList.ScrollIntoView(resultList.Items[0]);
                    }
                    else
                    {
                        synopsisResultList.ItemsSource = new string[] { "No similar animes found in our database :(" };
                    }
                }
                else
                {
                    d2aList.UnselectAll();
                }
            }
            catch (Exception ex)
            {
                resultList.ItemsSource = new string[] { };
                MessageBox.Show("Exception catched!", ex.Message, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

    }

}
