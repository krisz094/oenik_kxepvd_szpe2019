﻿using LemmaSharp.Classes;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerProgAnime.Entity
{
    public class Data1Anime
    {
        public static ConcurrentDictionary<string, int> nAnimesThatContainWord;
        public static int nData1Anime;

        static Data1Anime()
        {
            nAnimesThatContainWord = new ConcurrentDictionary<string, int>();
        }

        public static void CompareAndGroupIfSimilar(Data1Anime a1, Data1Anime a2)
        {
            var a1Importants = a1.getImportantWords();
            var a2Importants = a2.getImportantWords();
            var a1ImportantTop = a1Importants.Take((int)(a1Importants.Count() * 0.2)).Select(x => x.Key);
            var a2ImportantTop = a2Importants.Take((int)(a2Importants.Count() * 0.2)).Select(x => x.Key);
            var intersect = a1ImportantTop.Intersect(a2ImportantTop);
            var nSameWords = intersect.Count();
            if (nSameWords > 0)
            {
                a1.similars.Add(new KeyValuePair<Data1Anime, int>(a2, nSameWords));
                a2.similars.Add(new KeyValuePair<Data1Anime, int>(a1, nSameWords));
            }
        }

        public ushort ID { get; set; }
        public string Name { get; set; }
        public string Synopsis { get; set; }

        public Dictionary<string, int> bagOfWords;
        public ConcurrentBag<KeyValuePair<Data1Anime, int>> similars;

        int synopsisWordCountCache;
        List<KeyValuePair<string, double>> importantWordsCache;

        public Data1Anime()
        {
            similars = new ConcurrentBag<KeyValuePair<Data1Anime, int>>();
            bagOfWords = new Dictionary<string, int>();
            synopsisWordCountCache = -1;
            importantWordsCache = null;
        }

        public void calcBagOfWords(Lemmatizer lemmatizer)
        {
            var query = from word in Synopsis.Split(' ').Select(x => lemmatizer.Lemmatize(x))
                        where word.Trim() != ""
                        group word by word into grp
                        select new { key = grp.Key, cnt = grp.Count() };

            foreach (var wordWithCount in query)
            {
                bagOfWords.Add(wordWithCount.key, wordWithCount.cnt);
            }
        }



        public int synopsisWordCount()
        {
            if (synopsisWordCountCache == -1)
            {
                synopsisWordCountCache = bagOfWords.Values.Sum();
            }

            return synopsisWordCountCache;
        }

        public int wordCountInCurrent(string word)
        {
            bagOfWords.TryGetValue(word, out int res);
            return res;
        }

        public int howManyAnimesContainWord(string word)
        {
            nAnimesThatContainWord.TryGetValue(word, out int res);
            return res;
        }

        public double getImportanceOfWord(string word)
        {
            double tf = (double)wordCountInCurrent(word) / synopsisWordCount(); // jelenegiben hányszor van benne / jelenlegi synopsis wordjeinek száma
            double idf = Math.Log10((double)nData1Anime / howManyAnimesContainWord(word)); // 10es alapú logaritmus (hány doksi van/mennyiben fordul elő a szó)
            return tf * idf;
        }
        public List<KeyValuePair<string, double>> getImportantWords()
        {
            if (importantWordsCache == null)
            {
                importantWordsCache = bagOfWords.Keys
                    .Select(x => new KeyValuePair<string, double>(x, getImportanceOfWord(x)))
                    .OrderByDescending(x => x.Value)
                    .ToList();
            }
            return importantWordsCache;
        }
        public override string ToString()
        {
            return Name;
        }
    }
}
