﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PerProgAnime.Entity
{
    public class Data2Anime
    {
        Data2Anime()
        {
            //similars = new ConcurrentBag<int>();
            similars = new ConcurrentDictionary<ushort, int>();
        }
        public ushort anime_id { get; set; }
        public string name { get; set; }
        /// <summary>
        /// Key: anime_id, Value: how many others liked it
        /// </summary>
        public ConcurrentDictionary<ushort, int> similars { get; set; }
        private int inOtherAnimeSimilarListCount;
        public int InOtherAnimeSimilarListCount { get { return inOtherAnimeSimilarListCount; } }
        public void IncrementInOtherAnimeSimilarListCount()
        {
            Interlocked.Increment(ref inOtherAnimeSimilarListCount);
        }   

        public override string ToString()
        {
            return name;
        }
        //public float? rating { get; set; }
        //public string genre { get; set; }
        //public string type { get; set; }
        //public ushort episodes { get; set; }
        //public int members { get; set; }
        //public ConcurrentBag<int> similars { get; set; }
    }


}
