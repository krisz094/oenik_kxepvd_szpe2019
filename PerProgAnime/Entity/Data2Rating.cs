﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerProgAnime.Entity
{
    class Data2Rating
    {
        public int user_id { get; set; }
        public ushort anime_id { get; set; }
        public sbyte rating { get; set; }
    }
}
